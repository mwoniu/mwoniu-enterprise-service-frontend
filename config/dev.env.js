'use strict'
const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  TIMEOUT: 2500,
  AUTHPRE: '"mwoniu_"',
  API_URL: '"https://rsmessage.wtoip.com/api"',
});
