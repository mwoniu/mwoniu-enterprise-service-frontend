export default class Url {
  /**
   *  设置url参数
   *  @param name
   *              参数名称
   *  @param value
   *              参数值
   *  @param _location
   *              可选参数，替换的URL
   *  @returns {XML|*|string|{by}|void}
   */
  static changeUrlParam(name, value, _location) {
    let url = _location || window.location.href;
    let reg = eval('/(' + name + '=)([^&]*)/gi');
    return url.replace(reg, name + '=' + value);
  }

  /**
   * 删除url指定名称的参数
   *  @param name
   *              参数名称
   *  @param _location
   *              可选参数，替换的URL
   * @returns {XML|*|string|{by}|void}
   */
  static delUrlParam(name, _location) {
    let url = _location || window.location.href;
    let reg = eval('/(' + name + '=)([^&]*)/gi');
    return url.replace(reg, "");
  }

  /**
   * 添加url指定名称的参数
   * @param name
   *      参数名称
   * @param value
   *      参数值
   * @param _location
   *      可选参数，替换的URL
   * @returns {string}
   */
  static addUrlParam(name, value, _location) {
    let currentUrl = _location || window.location.href;
    if (/\?/g.test(currentUrl)) {
      if (/name=[-\w]{4,25}/g.test(currentUrl)) {
        currentUrl = currentUrl.replace(/name=[-\w]{4,25}/g, name + "=" + value);
      } else {
        currentUrl += "&" + name + "=" + value;
      }
    } else {
      currentUrl += "?" + name + "=" + value;
    }
    return currentUrl;
  }

  /**
   * 打开新的窗口，防止被浏览器拦截
   * @param url
   * @param id
   */
  static newWin(url, id) {

    let _a = document.createElement('a');
    _a.setAttribute('href', url);
    _a.setAttribute('target', '_blank');
    _a.setAttribute('id', id);
    // 防止反复添加
    if (!document.getElementById(id)) document.body.appendChild(_a);
    _a.click();
  }
  
};
