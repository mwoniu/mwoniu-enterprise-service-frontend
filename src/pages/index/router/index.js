import Vue from 'vue';
import iView from 'iview';
import Utils from '@/libs/utils';
import VueRouter from 'vue-router';
import routers from './routers';
import store from "../store";

Vue.use(VueRouter);

const routerConfig = {
  routes: routers
};

const router = new VueRouter(routerConfig);

//全局解析守卫
router.beforeResolve((to, from, next) => {
  iView.LoadingBar.start();
  next();
});

//全局前置守卫
router.beforeEach((to, from, next) => {
  //验证登录
  if (!['login', 'logout'].includes(to.name)) {
    if (!store.getters.auth.check()) {
      router.push({name: 'login'});
      next(false);
      return;
    }
  }
  Utils.title(to.meta.title);
  next();
});

//全局后置钩子
router.afterEach((to, from) => {
  iView.LoadingBar.finish();
});

export default router;
