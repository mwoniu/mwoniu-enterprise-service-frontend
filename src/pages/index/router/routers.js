//组件导入
import mainWrapper from '../views/_layout/mainWrapper';

//作为独立登录页面路由
export const loginRouter = {path: '/login', name: 'login', meta: {title: 'Login - 登录'}, component: resolve => require(['../views/auth/oauth'], resolve)};
export const logoutRouter = {path: '/logout', name: 'logout', meta: {title: 'Logout - 注销'}, component: resolve => require(['../views/auth/logout'], resolve)};

//作为独立锁屏页面路由
export const locking = {path: '/locking', name: 'locking', component: resolve => require(['components/_layout/main-wrapper/lockscreen/components/locking-page.vue'], resolve)};

//作为Main组件的子页面展示但是不在左侧菜单显示的路由写在otherRouter里
export const otherRouter = {
  path: '/', name: 'otherRouter', redirect: {name: 'home_index'}, component: mainWrapper, title: '主页', children:
    [
      {path: '/home', name: 'home_index', meta: {title: '主页',}, component: resolve => require(['../views/home'], resolve)},
      {path: 'message', title: '消息中心', name: 'message_index', component: resolve => require(['../views/message/message.vue'], resolve)}
    ]
};

//作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
  {
    path: '/access', icon: 'key', name: 'access', meta: {title: '测试模块', icon: 'social-tux'}, component: mainWrapper,
    children: [
      {
        path: 'sb1', meta: {title: '测试页面1', icon: 'social-vimeo'}, name: 'access_sb1', component: resolve => {
          require(['../views/sb/sb1'], resolve);
        },
      },
      {
        path: 'sb2', meta: {title: '测试页面2', icon: 'social-html5'}, name: 'access_sb2', component: resolve => {
          require(['../views/sb/sb2'], resolve);
        }
      }
    ]
  },
];

//所有上面定义的路由都要写在下面的routers里
export const routers = [
  locking,
  otherRouter,
  loginRouter,
  logoutRouter,
  ...appRouter,
];

//对外声明变量
export default routers;
