//导入相关依赖
import Vue from 'vue';
import iView from 'iview';
import App from './app.vue';
import store from './store';
import router from './router/index';
import axios from './api/http-utils';
import * as filters from '@/filters';
import {sync} from 'vuex-router-sync';
import * as directives from '@/directives';
import 'iview/dist/styles/iview.css';

//加载完整工具库
window._ = require('lodash');

// 将组件挂载到Vue.prototype上，以便使用
Vue.prototype.axios = axios;
Vue.prototype.iview = iView;
Vue.prototype.store = store;
Vue.prototype.router = router;

//注入相关插件
Vue.use(iView);

// 同步 - 存储，路由器
sync(store, router);

//注入相关过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
});

//注入相关指令
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
});

//构建应用
const app = new Vue({
  axios,
  store,
  router,
  ...App,
});

//声明对外接口
export default app;
