'use strict';
import axios from 'axios';

export default {
  getUserInfoByToken() {
    const req = axios.post('/auth/get_user');
    return req.then((response) => Promise.resolve(response.data));
  },
}
