import qs from "qs";
import iView from "iview";
import axios from "axios";
import Url from "@/libs/url";
import store from "../store";
import router from "../router";
import * as types from "../store/mutation-types";
import {AJAX_IS_LOADING, RECEIVE_ERRORS} from "../store/mutation-types";

//env 配置
const env_config = process.env;

//是否启用调试
const debug = env_config.DEBUG;

//md5-hex生成工具类
const md5Hex = require('md5-hex');

// axios 配置
axios.defaults.timeout = env_config.TIMEOUT;
axios.defaults.baseURL = env_config.API_URL;

//设置请求头
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

// http request 拦截
axios.interceptors.request.use(
  config => {

    //设置授权TOKEN
    config.headers.common['Authorization'] = `Bearer ${store.state.account.auth.token}`;

    //检测当前Ajax是否正在执行
    config.params = config.params || {};
    config.data = config.data || {};

    //请求前配置
    config._nodefaultmsg = ((!_.isUndefined(config.params) && !_.isUndefined(config.params._nodefaultmsg) && config.params._nodefaultmsg) || (!_.isUndefined(config.data) && !_.isUndefined(config.data._nodefaultmsg) && config.data._nodefaultmsg) || false);
    config._noloading = ((!_.isUndefined(config.params) && !_.isUndefined(config.params._noloading) && config.params._noloading) || (!_.isUndefined(config.data) && !_.isUndefined(config.data._noloading) && config.data._noloading) || false);
    config._nohandlerror = ((!_.isUndefined(config.params) && !_.isUndefined(config.params._nohandlerror) && config.params._nohandlerror) || (!_.isUndefined(config.data) && !_.isUndefined(config.data._nohandlerror) && config.data._nohandlerror) || false);

    //存储标识、标识当前Ajax正在执行
    config.hash = md5Hex(config.url + config.method + qs.stringify(config.params) + qs.stringify(config.data));
    if (store.state.application.ajax_is_loading[config.hash]) {
      return Promise.reject({
        config: config,
        response: {
          data: {
            _nodefaultmsg: true,
            errmsg: '操作过快'
          }
        }
      });
    }
    store.commit('AJAX_IS_LOADING', {key: config.hash, value: true});

    //通用配置参数
    if (!_.isUndefined(config.params) && !_.isUndefined(config.params._nodefaultmsg)) delete config.params._nodefaultmsg;
    if (!_.isUndefined(config.params) && !_.isUndefined(config.params._noloading)) delete config.params._noloading;
    if (!_.isUndefined(config.params) && !_.isUndefined(config.params._nohandlerror)) delete config.params._nohandlerror;
    if (!_.isUndefined(config.data) && !_.isUndefined(config.data._nodefaultmsg)) delete config.data._nodefaultmsg;
    if (!_.isUndefined(config.data) && !_.isUndefined(config.data._noloading)) delete config.data._noloading;
    if (!_.isUndefined(config.data) && !_.isUndefined(config.data._nohandlerror)) delete config.data._nohandlerror;

    //Post参数转换
    if (_.isEqual("post", config.method) && !_.isUndefined(config.data) && _.isObject(config.data)) {
      config.data = qs.stringify(config.data);
    }

    //状态条
    if (!config._noloading) iView.LoadingBar.start();

    //配置信息
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

// http response 拦截器
axios.interceptors.response.use(
  response => {

    //配置信息
    let config = response.config;

    //标识Ajax执行完成
    if (config && config.hash) {
      setTimeout(() => store.commit(AJAX_IS_LOADING, {key: config.hash, value: false}), 500);
    }

    //响应结果
    let res = response.data;

    //调试日志
    if (debug) {
      console.debug(
        `${config.method} : ${config.url}`,
        "\n------------------------------------------------\n",
        res
      );
    }

    //正确结果
    if (!_.isUndefined(res.success) && res.success == true) {
      if (!config._noloading) iView.LoadingBar.finish();
    } else {
      if (config._nohandlerror) return response;
      if (!config._noloading) iView.LoadingBar.error();
      store.commit(RECEIVE_ERRORS, res);
      return Promise.reject(response.data)
    }
    return response;
  },
  error => {
    let config = error.config;

    //标识Ajax执行完成
    if (config && config.hash) {
      setTimeout(() => store.commit(AJAX_IS_LOADING, {key: config.hash, value: false}), 500);
    }

    //是否进行全局处理错误信息
    if (config._nohandlerror) return;

    //全局错误信息处理
    if (error.response) {
      switch (error.response.status) {
        case 401:
          console.log("401 清除token信息并跳转到登录页面");
          store.commit(types.LOGOUT_SUCCESS);
          return router.push({
            name: 'auth_login',
            query: {redirect_url: Url.delUrlParam("token", window.location.href)}
          });
        case 429:
          error.response.data = Object.assign(error.response.data, {errmsg: error.response.data.errmsg || '您的请求过于频繁'});
          break;
        case 403:
          error.response.data = Object.assign(error.response.data, {errmsg: error.response.data.errmsg || '您的网络请求被拒绝'});
          break;
        case 502:
          error.response.data = Object.assign(error.response.data, {errmsg: error.response.data.errmsg || '服务器繁忙'});
          break;
        case 500:
          error.response.data = Object.assign(error.response.data, {errmsg: error.response.data.errmsg || '服务器内部'});
          break;
        case 404:
          error.response.data = Object.assign(error.response.data, {errmsg: error.response.data.errmsg || '您的请求地址不存在'});
          break;
      }

      //错误信息包装
      error.response.data = Object.assign({
        _nodefaultmsg: config._nodefaultmsg,
        errmsg: '网络繁忙'
      }, error.response.data);

    }

    //是否开启调试
    if (debug) console.debug(error);
    if (!config._noloading) iView.LoadingBar.error();
    error.data = (_.isUndefined(error.response) || _.isUndefined(error.response.data)) ? {errmsg: "程序内部异常，请速联系管理员处理"} : error.response.data;
    store.commit(RECEIVE_ERRORS, error.data);
    return Promise.reject(error.data);

  }
);

export default axios;
