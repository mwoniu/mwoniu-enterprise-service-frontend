import {appRouter, otherRouter} from "../../router/routers";

export default {
  //语言
  lang: '',
  //缓存的页面
  cachePage: [],
  //是否全屏
  isFullScreen: false,
  //要展开的菜单数组
  openedSubmenuArr: [],
  //菜单主题
  menuTheme: 'dark',
  //主体颜色
  themeColor: '',
  //当前打开的页面名称
  currentPageName: '',
  //当前面包屑数组
  currentPath: [],
  //菜单列表
  menuList: [...appRouter],
  //路由列表
  routers: [otherRouter, ...appRouter],
  //消息数目
  messageCount: 0,
}
