import {LOGIN_SUCCESS, LOGOUT_SUCCESS, RECEIVE_LOGIN_TOKEN} from "../mutation-types";

export const receiveLoginToken = ({commit}, token) => {
  commit(RECEIVE_LOGIN_TOKEN, token);
  return new Promise((resolve) => resolve());
};

export const login = ({commit}, userInfo) => {
  commit(LOGIN_SUCCESS, userInfo);
  return new Promise((resolve) => resolve());
};

export const logout = ({commit}) => {
  commit(LOGOUT_SUCCESS);
  return new Promise((resolve) => resolve());
};
