import user from "../../api/user";
import {LOGIN_SUCCESS} from "../mutation-types";

export const getUserInfoByToken = ({commit}) => {
  const req = user.getUserInfoByToken().then((resp) => {
    commit(LOGIN_SUCCESS, resp.data);
    return Promise.resolve(resp);
  }).catch((error) => {
    return Promise.reject(error);
  });
  return req;
};
