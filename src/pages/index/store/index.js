import Vue from "vue";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";
import states from "./states";
import * as getters from "./getters";
import * as actions from "./actions";
import * as modules from "./modules";

//应用组件
Vue.use(Vuex);

//是否非生产环境
const debug = process.env.NODE_ENV !== 'production';

//声明对外接口
export default new Vuex.Store({
  //设置状态
  state: states,
  //计算属性
  getters: getters,
  //设置动作
  actions: actions,
  //设置模块
  modules: modules,
  //严格模式
  strict: false,
  //设置插件
  plugins: debug ? [createLogger()] : []
});
