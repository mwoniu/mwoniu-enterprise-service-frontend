import {LOGIN_SUCCESS, LOGOUT_SUCCESS, RECEIVE_LOGIN_TOKEN} from "../mutation-types";
import _ from "lodash";

export const account = {
  state: {
    auth: {
      sex: sessionStorage.getItem(`${process.env.AUTHPRE}sex`),
      token: sessionStorage.getItem(`${process.env.AUTHPRE}token`),
      userid: sessionStorage.getItem(`${process.env.AUTHPRE}userid`),
      avatar: sessionStorage.getItem(`${process.env.AUTHPRE}avatar`),
      account: sessionStorage.getItem(`${process.env.AUTHPRE}account`),
      nickname: sessionStorage.getItem(`${process.env.AUTHPRE}nickname`),
    }
  },
  getters: {
    auth: state => {
      Object.assign(state.auth, {
        //判断是否已登录
        check() {
          return this.token !== null && this.token !== "";
        },
      });
      return state.auth
    }
  },
  mutations: {
    [RECEIVE_LOGIN_TOKEN](state, token) {
      Object.assign(state.auth, {token: token});
      sessionStorage.setItem(`${process.env.AUTHPRE}token`, token);
    },
    [LOGIN_SUCCESS](state, user) {
      console.log("顺畅的登录系统！");
      Object.assign(state.auth, user);
      sessionStorage.setItem(`${process.env.AUTHPRE}sex`, user.sex);
      sessionStorage.setItem(`${process.env.AUTHPRE}userid`, user.userid);
      sessionStorage.setItem(`${process.env.AUTHPRE}account`, user.account);
      sessionStorage.setItem(`${process.env.AUTHPRE}nickname`, !_.isUndefined(user.fullname) ? user.fullname : "匿名");
      sessionStorage.setItem(`${process.env.AUTHPRE}avatar`, !_.isUndefined(user.picture) ? user.picture : "/static/images/default/avatar.png");
    },
    [LOGOUT_SUCCESS](state) {
      console.log("愉快的登出系统！");
      Object.assign(state, {
        auth: {
          token: null
        }
      });
      localStorage.clear();
      sessionStorage.clear();
    }
  }
};
