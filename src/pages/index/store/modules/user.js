import {RECEIVE_USER_INFO,} from "../mutation-types";

export const user = {
  state: {
    userInfo: null,
    userList: []
  },
  mutations: {
    [RECEIVE_USER_INFO](state, userInfo) {
      Object.assign(state, {user: userInfo})
    },
  }
};
