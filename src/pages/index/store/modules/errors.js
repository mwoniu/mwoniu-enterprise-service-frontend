import iView from "iview";
import {RECEIVE_ERRORS} from "../mutation-types";

export const errors = {
  state: {},
  mutations: {
    [RECEIVE_ERRORS](state, data) {
      let message = '';
      if (_.isString(data)) data = {errmsg: data};
      if (!_.isUndefined(data) && data.errmsg) message = `${data.errmsg} `;
      if (!_.isUndefined(data) && data.errors) {
        data.errors.forEach((value) => {
          message += `${value.errmsg}\n`;
        })
      }
      if (message === '') message = '未知错误！';
      Object.assign(state, {data: data});
      if (!data._nodefaultmsg) iView.Notice.error({title: message});
    }
  }
}
