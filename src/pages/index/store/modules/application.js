import {AJAX_IS_LOADING} from "../mutation-types";

export const application = {
  state: {
    ajax_is_loading: {},
  },
  getters: {
    ajax_is_loading: state => {
      return state.ajax_is_loading;
    },
  },
  mutations: {
    [AJAX_IS_LOADING](state, data) {
      if (state.ajax_is_loading && data) {
        window.ajax_is_loading = data.value;
        state.ajax_is_loading[data.key] = data.value;
      }
    }
  }
};
